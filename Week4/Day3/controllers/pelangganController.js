const connection = require('../models/connection.js')

class PelangganController {
    async getAll(req, res) {
        try {
            var sql = "select p.id, p.nama from pelanggan p order by p.id"

            connection.query(sql, function(err, result) {
                if (err) throw err

                res.json({
                    status: "success",
                    data: result
                })
            })
        } catch (e) {
            res.json({
                status: "error"
            })
        }
    }

    async getOne(req, res) {
        try {
            var sql = "select p.id, p.nama from pelanggan p where p.id = ?"

            connection.query(sql, [req.params.id], function(err, result) {
                if (err) throw err

                res.json({
                    status: "success",
                    data: result[0]
                })
            })
        } catch (e) {
            res.json({
                status: "error"
            })
        }
    }

    async create(req, res) {
        try {

            var sql = 'INSERT INTO pelanggan(nama) VALUES (?)'

            connection.query(sql, [req.body.nama], (err, result) => {
                if (err) {
                    res.json({
                    status: "Error",
                    error: err
                    });
                }
                
                var sqlSelect = 'select p.id, p.nama from pelanggan p where p.id = ?'

                connection.query(sqlSelect, [result.insertId], function(err, result) {
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        })
                    }

                    res.json({
                        status: 'Success',
                        data: result[0]
                    })
                })
            })
        } catch (err) {
        
            res.json({
            status: "Error",
            error: err
            })
        }
    }

    async update(req, res) {
        try {

            var sql = 'UPDATE pelanggan p SET nama = ? WHERE id = ?'

            connection.query(sql, [req.body.nama, req.params.id], (err, result) => {
                if (err) {
                    res.json({
                    status: "Error",
                    error: err
                    });
                }

                var sqlSelect = 'select p.id, p.nama from pelanggan p where p.id = ?'

                connection.query(sqlSelect, [req.params.id], function(err, result) {
                    if(err) {
                        res.json({
                            status: "Error",
                            error: err
                        })
                    }
                
                    res.json({
                    status: 'Success',
                    data: result[0]
                    })
                    
                })
            })
        } catch (err) {
            
            res.json({
            status: "Error",
            error: err
            })
        }
    }

    async delete(req, res) {
        try {

            var sql = 'DELETE FROM pelanggan p WHERE id = ?'

            connection.query(sql, [req.params.id], (err, result) => {
                if (err) {
                    res.json({
                    status: "Error",
                    error: err
                    });
                } 

                
                res.json({
                status: 'Success',
                data: result
                })
            })
        } catch (err) {
            
                res.json({
                status: "Error",
                error: err
                })
        }
    }
}

module.exports = new PelangganController