const { Pelanggan } = require("../../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator')

module.exports = {
    create: [

        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        },
    ],
    update: [
        
        check('id').isNumeric().custom(value=> {
            return Pelanggan.findOne({
              where: {
                id: value
              }
            }).then(p => {
              if(!p) {
                throw new Error('ID pelanggan tidak ada!')
              }
            })
        }),
        check('nama').isString().notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        },
    ],
}