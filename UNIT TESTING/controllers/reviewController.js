const {
  review,
  movie,
  user
} = require('../models');
const {
  ObjectId
} = require('mongodb')

class ReviewController {

  async create(req, res) {
    const data = await Promise.all([
      movie.findOne({
        _id: req.body.movie_id
      }),
      user.findOne({
        _id: req.user._id
      })
    ])
    review.create({
      comment: req.body.comment,
      rating: req.body.rating,
      movie: data[0],
      user: data[1]
    }).then(result => {
      res.json({
        status: "Review created",
        data: {
          comment: result.comment,
          rating: result.rating,
          user: result.user.fullName,
          user: result.user.email,
          user: result.user.image,
          movie: result.movie.title,
          movie: result.movie.poster
        }
      })
    })
  };

  async showByMovies(req, res) {
    let movieObj = new ObjectId(req.query.movie_id);
    const filReview = await review
      .find({
        "movie._id": movieObj,
      }, 'comment rating')
    try {
      const {
        page = 1, limit = 10
      } = req.query;
      review.findOne({
        "movie._id": req.param.movie_id
      })
      const filReviewe = await review
        .find({
          "movie._id": movieObj,
        }, 'comment rating')
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
      const count = await filReview.length;
      res.json({
        filReviewe,
        totalPages: Math.ceil(count / limit),
        currentPage: page
      })
    } catch (err) {
      console.log(err.message);
    }
  };
  async showByUser(req, res) {
    let userObj = new ObjectId(req.query.user_id);
    const filReviewb = await review
      .find({
        "user._id": userObj,
      })
    try {
      const {
        page = 1, limit = 10
      } = req.query;
      review.findOne({
        "user._id": req.param.user_id
      });
      const filReviewd = await review
        .find({
          "user._id": userObj,
        }, 'comment rating movie._id user._id')
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
      const count = await filReviewb.length;
      res.json({
        filReviewd,
        totalPages: Math.ceil(count / limit),
        currentPage: page
      })
    } catch (err) {
      console.log(err.message);
    }
  };
  async getRatingMovie(req, res) {
    let movieObj = new ObjectId(req.query.movie_id);
    const filReviewc = await review
      .find({
        "movie._id": movieObj
      })
    let sumAverage = 0;
    await filReviewc.forEach((item, i) => {
      sumAverage += item.rating
    });
    const averageRating = sumAverage / filReviewc.length
    res.json({
      data: averageRating
    })
  };
  async showOne(req, res) {
    review.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "success get review",
        data: {
          comment: result.comment,
          rating: result.rating,
          user: result.user.fullName,
          user: result.user.email,
          user: result.user.image,
          movie: result.movie.title,
          movie: result.movie.poster
        }
      })
    });
  };
  async update(req, res) {
    review.findOneAndUpdate({
        _id: req.params.id
      }, {
        comment: req.body.comment,
        rating: req.body.rating,
      }).then(() => {
        return review.findOne({
          _id: req.params.id
        })
      })
      .then(result => {
        res.status(200).json({
          message: "Review update success"
        });
      });
  };
  async delete(req, res) {
    review.delete({
      _id: req.params.id
    }).then(() => {
      res.status(200).json({
        message: "review deleted",
        data: null
      });
    });
  };
};

module.exports = new ReviewController;
