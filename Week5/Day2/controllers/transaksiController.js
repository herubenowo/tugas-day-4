const { ObjectId } = require('mongodb')
const client = require('../models/connection.js')

class TransaksiController {

    async getAll(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.find({}).toArray().then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        const barang = await penjualan.collection('barang').findOne({
            _id: new ObjectId(req.body.id_barang)
        })

        const pelanggan = await penjualan.collection('pelanggan').findOne({
            _id: new ObjectId(req.body.id_pelanggan)
        })

        let total = eval(barang.harga.toString()) * req.body.jumlah

        transaksi.insertOne({
            barang: barang,
            pelanggan: pelanggan,
            jumlah: req.body.jumlah,
            total: total
        }).then(result => {
            res.json({
                status: "new data added to table transaksi",
                data: result.ops[0]
            })
        })
    }

    async update(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        const barang = await penjualan.collection('barang').findOne({
            _id: new ObjectId(req.body.id_barang)
        })

        const pelanggan = await penjualan.collection('pelanggan').findOne({
            _id: new ObjectId(req.body.id_pelanggan)
        })

        let total = eval(barang.harga.toString()) * req.body.jumlah

        transaksi.updateOne({
            _id: new ObjectId(req.params.id)
        }, {
            $set: {
                barang: barang,
                pelanggan: pelanggan,
                jumlah: req.body.jumlah,
                total: total
            }
        }).then(() => {
            return transaksi.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
            res.json({
                status: "table transaksi's data have been sucessfully updated",
                data: result
            })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "one of table transaksi's data have been successfully deleted",
            })
        })
    }
}

module.exports = new TransaksiController