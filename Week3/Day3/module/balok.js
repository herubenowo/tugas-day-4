// Assignment Week3 - Day3 by : Heru Benowo
// import BangunRuang
const bangunruang = require(`./bangunruang.js`)

// Make balok class
class Balok extends bangunruang {
    constructor(panjang, lebar, tinggi) {
        super(`Balok`)

        this.panjang = panjang
        this.lebar = lebar
        this.tinggi = tinggi
    }

    // Override menghitung luas bangunruang
    menghitungluaspermukaan() {
        return (2 * (this.panjang * this.lebar)) + (2 * (this.panjang * this.tinggi)) + (2 * (this.lebar * this.tinggi))
    }
    
    // Override menghitung volume bangunruang
    menghitungvolume() {
        return this.panjang * this.lebar * this.tinggi
    }
}

module.exports = Balok;