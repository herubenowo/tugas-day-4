// Import bangun class
const bangun = require(`./bangun.js`);

// this is class BangunRuang
class BangunRuang extends bangun {
    constructor(name) {
        super(name)
    
        // It is abstract class
        if (this.constructor === BangunRuang) {
            throw new Error(`This is abstract`)
        }
    }

    // This is private mod, so you can't access on another file js
    #hello() {
        console.log(`Hello Bangun Ruang!`);
    }

    // Instance method
    menghitungluaspermukaan() {
        console.log(`Menghitung Luas Permukaan`);
    }

    // Instance method
    menghitungvolume() {
        console.log(`Menghitung Volume`);
    }
}

module.exports = BangunRuang;