const { user } = require('../models');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')

class UserController {

    // CONTROLLER FOR SIGNUP
    async signup(req, res) {
        // GETTING USER DATA FROM AUTH
        const body = {
            _id: req.user._id,
            email: req.user.email
        };
        // DECLARING TOKEN BY ID
        const token = await jwt.sign({
            user: body
        }, 'secret_key');
        res.status(200).json({
            message: "Signup success!",
            token: token
        });
    };

    // CONTROLLER FOR LOGIN
    async login(req, res) {
        const body = {
            _id: req.user._id,
            email: req.user.email
        };

        const token = await jwt.sign({
            user: body
        }, 'secret_key');

        res.status(200).json({
            message: "Login success!",
            token: token
        });
    };

    // CONTROLLER FOR GETTING PROFILE
    async getProfile(req, res) {
        user.findOne({
            _id: req.user._id
        }).then(result => {
            res.status(200).json({
                message: "success",
                data: {
                    fullName: result.fullName,
                    email: result.email,
                    role: result.role,
                    image: result.image
                }
            })
        })
    }

    // CONTROLLER FOR UPDATING PROFILE NAME
    async updateProfileName(req, res) {
        user.findOneAndUpdate({
            _id: req.user._id
        }, {
            fullName: req.body.fullName
        }).then(result => {
            res.status(200).json({
                message: "Profile name update success"
            });
        });
    };

    // CONTROLLER FOR UPDATING PROFILE PASSWORD
    async updateProfilePassword(req, res) {
        user.findOneAndUpdate({
            _id: req.user._id
        }, {
            password: req.body.password
        }).then(result => {
            res.status(200).json({
                message: "Password update success"
            });
        });
    };

    // CONTROLLER FOR UPDATING PHOTO
    async updateProfilePhoto(req, res) {
        user.findOneAndUpdate({
            _id: req.user._id
        }, {
            image: req.file === undefined ? "" : req.file.filename
        }).then(result => {
            res.json({
                status: "Profile photo update success"
            });
        });
    };

    // CONTROLLER FOR DELETING PROFILE
    async deleteProfile(req, res) {
        user.delete({
            _id: req.user._id
        }).then(() => {
            res.status(200).json({
                message: "Profile delete success",
                data: null
            });
        });
    };
};

module.exports = new UserController;