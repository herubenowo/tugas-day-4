const mongoose = require('mongoose');

// DATABASE HOST
const uri = "mongodb://localhost:27017/miniproject_dev"; 

// CONNECTING TO DATABASE
mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true });

// IMPORTING USER.JS
const user = require('./user.js');

// EXPORTING USER
module.exports = { user };