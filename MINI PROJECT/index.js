const express = require('express');
const app = express();
const bodyParser = require('body-parser'); 
const userRoutes = require('./routes/userRoutes.js')

// SET BODY PARSER FOR HTTP POST OPERATION
app.use(bodyParser.json()); // SUPPORT JSON ENCODED BODIES
app.use(bodyParser.urlencoded({
  extended: true
})); // SUPPORT ENCODED BODIES

// SET STATUS ASSETS TO PUBLIC DIR
app.use(express.static('public'));

app.use('/user', userRoutes) // IF ACCESSING LOCALHOST:6969/user/ WE WILL GO TO USER ROUTES

app.listen(6969, () => console.log("server running on http://localhost:6969"))